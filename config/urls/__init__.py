from django.contrib import admin
from django.urls import include, path

from .api_versions import urlpatterns as api_urlpatterns

urlpatterns = [
    path("admin/", admin.site.urls),
    path("main/", include("apps.users.urls")),
]

urlpatterns += api_urlpatterns
