from django import views
from django.shortcuts import render
from django.http import HttpResponseBadRequest
from . import forms, services
from django.views import generic


class MainView(generic.FormView):
    """Display main page with calc form."""
    form_class = forms.LoanForm
    success_url = "/result/"
    template_name: str = "index.html"

    def post(self, request, *args, **kwargs):
        data_form: forms.LoanForm = self.get_form()
        if not data_form.is_valid():
            return HttpResponseBadRequest("Invalid data")
        calc_service = services.BankInterest(
            summ=data_form.cleaned_data["loan"],
            period=data_form.cleaned_data["period"],
            perc=data_form.cleaned_data["interest"],
        )
        res = calc_service.ann_int()
        annuity_result = {
            "monthly_payment": res[0],
            "overall_total": res[1],
        }
        res = calc_service.diff_int()
        diff_result = {
            "monthly_payments": res[0],
            "overall_total": res[1],
        }
        annuity_form = forms.AnnuityLoanResultForm(data=annuity_result)
        diff_form = forms.DifferentiatedLoanResultForm(data=diff_result)

        if not annuity_form.is_valid() and diff_form.is_valid():
            return HttpResponseBadRequest("Calc problems")

        context = {"annuity_form": annuity_form, "diff_form": diff_form}
        return render(request, "result.html", context=context)
    

class LoanCalcView(generic.FormView):
    """Calculate loan percents."""
    template_name: str = "templates/result.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["annuity_form"] = self.get_form()
        return context
    