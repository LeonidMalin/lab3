from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.contrib.postgres.forms import SimpleArrayField
from django import forms

from .models import User

__all__ = ('CustomUserChangeForm', 'CustomUserCreationForm')


class CustomUserChangeForm(UserChangeForm):
    """Custom change form for ``User`` model.

    This class overrides ``UserChangeForm`` to provide different ``User``
    model in Meta.

    """

    class Meta:
        model = User
        fields = '__all__'


class CustomUserCreationForm(UserCreationForm):
    """Custom creation form for ``User`` model.

    This class overrides ``UserCreationForm`` to provide different ``User``
    model in Meta and also replaces ``username`` field with ``email`` field.

    """

    class Meta:
        model = User
        fields = ('email',)


class LoanForm(forms.Form):
    """Form to calculate a loan."""
    loan = forms.IntegerField(
        min_value=1,
        max_value=1_000_000_000,
        required=True,
        label="Loan amount",
    )
    period = forms.IntegerField(
        min_value=1,
        max_value=100,
        required=True,
        label="Period in years",
    )
    interest = forms.FloatField(
        min_value=1,
        max_value=100,
        required=True,
        label="Interest per month",
    )


class AnnuityLoanResultForm(forms.Form):
    """Display annuity loan calculation result."""
    monthly_payment = forms.DecimalField(
        min_value=1,
        label="Monthly payment",
        # disabled=True,
    )
    overall_total = forms.DecimalField(
        min_value=1,
        label="Overall total",
        # disabled=True,
    )


class DifferentiatedLoanResultForm(forms.Form):
    """Display differentiated result."""
    monthly_payments = SimpleArrayField(
        base_field=forms.FloatField(),
        label="Monthly payments",
        # disabled=True,
    )
    overall_total = forms.DecimalField(
        min_value=1,
        label="Overall total",
        # disabled=True,
    )
