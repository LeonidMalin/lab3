import pytest
from .. import services

from typing import Tuple, List

@pytest.mark.parametrize(
    argnames=["loan", "period", "interest", "expected_result"],
    argvalues=[
        [1000000, 10, 15, (16133.5, 1936019.48)],
        [99999, 3, 10, (3226.69, 116160.71)],
    ],
)
def test_loan_annuity_formula(
    loan: int,
    period: int,
    interest: int,
    expected_result: Tuple[float, float],
):
    service = services.BankInterest(
        summ=loan,
        period=period,
        perc=interest,
    )
    result = service.ann_int()
    assert result == expected_result


@pytest.mark.parametrize(
    argnames=["loan", "period", "interest", "expected_result"],
    argvalues=[
        [
            10000,
            1,
            10,
            ([916.67,909.72,902.78,895.83,888.89,881.94,875.0,868.06,861.11,854.17,847.22,840.28], 10541.67),
        ],
        [
            123456,
            1,
            11,
            ([11419.68,11325.37,11231.07,11136.76,11042.45,10948.15,10853.84,10759.53,10665.23,10570.92,10476.61,10382.31], 130811.92),
        ],
    ],
)
def test_loan_diff_formula(
    loan: int,
    period: int,
    interest: int,
    expected_result: Tuple[List[float], float],
):
    service = services.BankInterest(
        summ=loan,
        period=period,
        perc=interest,
    )
    result = service.diff_int()
    assert result == expected_result
